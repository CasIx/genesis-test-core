import { ICity } from "./city.model";
import { ITransport, TransportType } from "./transport.model";

export enum Currency {
    USD = 'usd'
}

export enum RouteStatus {
    Pending ='pending',
    InProgress = 'in-progress',
    Finished = 'finished',
}

export interface IRoute {
    id: number;
    status: RouteStatus;
    startCity: ICity;
    startCityId: number;
    finishCity: ICity;
    finishCityId: number;
    distance: number;
    transportType: TransportType; 
    estimatedRevenue: number;
    revenueCurrency: Currency;
    transport: ITransport;
    startDate: Date;
    finishDate: Date;
}