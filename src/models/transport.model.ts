export enum TransportType {
    Freight = 'freight',
    Carriage = 'carriage', 
    Cars = 'cars',
}

export enum TransportStatus {
    Free = 'free',
    Busy = 'busy',
}

export interface ITransport {
    id: number;
    number: string;
    model: string;
    type: TransportType;
    mileage: number;
    status: TransportStatus;
}
