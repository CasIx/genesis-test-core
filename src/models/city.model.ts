export interface ICity {
    id: number;
    name: string;
    lat: number;
    lng: number;
}